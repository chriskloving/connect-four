let playerOne = document.getElementById("redToken");
let playerTwo = document.getElementById("blueToken");
let boardDiv = document.getElementById("boardDiv");
let currentCell;
let currentPlayer = playerOne;

const board = [
  [null, null, null, null, null, null],
  [null, null, null, null, null, null],
  [null, null, null, null, null, null],
  [null, null, null, null, null, null],
  [null, null, null, null, null, null],
  [null, null, null, null, null, null],
  [null, null, null, null, null, null]
];
const colOneDiv = document.createElement("div");
colOneDiv.classList = "colDiv";
colOneDiv.id = "colOneDiv";
const colTwoDiv = document.createElement("div");
colTwoDiv.classList = "colDiv";
colTwoDiv.id = "colTwoDiv";
const colThreeDiv = document.createElement("div");
colThreeDiv.classList = "colDiv";
colThreeDiv.id = "colThreeDiv";
const colFourDiv = document.createElement("div");
colFourDiv.classList = "colDiv";
colFourDiv.id = "colFourDiv";
const colFiveDiv = document.createElement("div");
colFiveDiv.classList = "colDiv";
colFiveDiv.id = "colFiveDiv";
const colSixDiv = document.createElement("div");
colSixDiv.classList = "colDiv";
colSixDiv.id = "colSixDiv";
const colSevenDiv = document.createElement("div");
colSevenDiv.classList = "colDiv";
colSevenDiv.id = "colSevenDiv";
const columns = [
  colOneDiv,
  colTwoDiv,
  colThreeDiv,
  colFourDiv,
  colFiveDiv,
  colSixDiv,
  colSevenDiv
];
const columnsId = [
  "colOneDiv",
  "colTwoDiv",
  "colThreeDiv",
  "colFourDiv",
  "colFiveDiv",
  "colSixDiv",
  "colSevenDiv"
];
for (let i = 0; i < columns.length; i++) {
  boardDiv.appendChild(columns[i]);
}
for (let i = 0; i < columns.length; i++) {
  columns[i].addEventListener("click", handleClick);
}

function addValueToBoard(columnIndex) {
  let value;
  if (currentPlayer === playerOne) {
    value = 1;
  } else if (currentPlayer === playerTwo) {
    value = 2;
  }

  let columnAddition = board[columnIndex];
  let cellIndex = columnAddition.indexOf(null);
  columnAddition[cellIndex] = value;
}

function findVerticalWin() {
  for (let i = 0; i < board.length; i++) {
    for (let j = 0; j < 3; j++) {
      if (
        board[i][j] !== null &&
        board[i][j] === board[i][j + 1] &&
        board[i][j] === board[i][j + 2]
      ) {
        if (board[i][j] === board[i][j + 3]) {
          alert("verticalYou Win!!!");
          window.location.reload();
        }
      }
    }
  }
}

function findHorizontalWin() {
  for (let i = 0; i < 4; i++) {
    for (let j = 0; j < board.length; j++) {
      if (board[i][j] == 1 || board[i][j] == 2) {
        if (
          board[i][j] === board[i + 1][j] &&
          board[i][j] === board[i + 2][j]
        ) {
          if (board[i][j] === board[i + 3][j]) {
            alert(" horizontalYou Win!");
            window.location.reload();
          }
        }
      }
    }
  }
}

function findDownRightDiagnolWin() {
  for (let i = 0; i < 4; i++) {
    for (let j = 0; j < 3; j++) {
      if (
        board[i][j] !== null &&
        board[i][j] === board[i + 1][j + 1] &&
        board[i][j] === board[i + 2][j + 2]
      ) {
        if (board[i][j] === board[i + 3][j + 3]) {
          alert("diagnolDownRightDiagnolYou Win!!!");
          window.location.reload();
        }
      }
    }
  }
}

function findDownLeftDiagnolWin() {
  for (let i = 0; i < 4; i++) {
    for (let j = 3; j < board.length; j++) {
      if (
        board[i][j] !== null &&
        board[i][j] === board[i + 1][j - 1] &&
        board[i][j] === board[i + 2][j - 2]
      ) {
        if (board[i][j] === board[i + 3][j - 3]) {
          alert("diagnolDownLeftYou Win!!!");
          window.location.reload();
        }
      }
    }
  }
}
function findTie() {
    if(board[0][5] && board[1][5] && board[2][5] && board[3][5] && board[4][5] && board[5][5] && board[6][5]) {
        alert("Tie Game!!!");
        window.location.reload();

    }    
}

function handleClick(event) {
  let selectedCol = event.currentTarget;
  //console.log(selectedCol.id);

  let colIndex = columnsId.indexOf(selectedCol.id);

  if (selectedCol.childElementCount <= 5) {
    addValueToBoard(colIndex);
    console.log(board);

    if (currentPlayer === playerOne) {
      let redToken = document.createElement("div");
      redToken.classList.add("redToken");
      selectedCol.appendChild(redToken);
      currentPlayer = playerTwo;
    } else if (currentPlayer === playerTwo) {
      let blueToken = document.createElement("div");
      blueToken.classList.add("blueToken");
      selectedCol.appendChild(blueToken);
      currentPlayer = playerOne;
    }
  }
  findVerticalWin();
  findHorizontalWin();
  findDownLeftDiagnolWin();
  findDownRightDiagnolWin();
  findTie();
}
